﻿using Hotel.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hotel.Presentation.Guest_and_Agents
{
    public partial class Modification : Form
    {
        private readonly GuestService _guestService;
        public Modification(int ID)
        {
            
            InitializeComponent();
            _guestService = new GuestService();
            txtID.Name = ID.ToString();
    }


        private void Modification_Load(object sender, EventArgs e)
        {
            Clear();
            FillData(Convert.ToInt32(txtID.Name));

        }
        public void FillData(int ID)
        {
            var guest = _guestService.GetGuests().Where(x => x.ID == ID).FirstOrDefault();
            txtID.Text = guest.ID.ToString();
            txtName.Text = guest.Name;
            txtLastName.Text = guest.LastName;
            txtAdress.Text = guest.Address;
            txtPhone.Text = guest.PhoneNr;
            txtAge.Text = guest.Age.ToString();
            txtGender.Text = guest.Gender;
            txtCountry.Text = guest.Country;
            txtCity.Text = guest.City;
            txtHotelRoom.Text = guest.HotelRoom.ToString();
            txtArrivalDate.Text = guest.ArrivalDate;
            txtDepartureDate.Text = guest.DepartureDate;
            txtEmail.Text = guest.Email;
            btnSave.Text = "Update";
            btnDelete.Enabled = true;

        }
        void Clear()
        {
            txtName.Text = txtLastName.Text = txtAdress.Text = txtPhone.Text = txtGender.Text = txtCountry.Text = txtCity.Text = txtArrivalDate.Text = txtDepartureDate.Text = txtEmail.Text = txtAge.Text = txtHotelRoom.Text = txtID.Text = null;
            btnDelete.Enabled = false;
            btnSave.Text = "Save";
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
        var guest = _guestService.GetGuests().FirstOrDefault();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
           
            if (MessageBox.Show("Ar you sure?", "Guest Deleting", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _guestService.Delete(Convert.ToInt32(txtID));
                MessageBox.Show("Guest deleted successful");
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            var guest = _guestService.GetGuests().FirstOrDefault();
            guest.ID = Convert.ToInt32(txtID.Text);
            guest.Name = txtName.Text.Trim();
            guest.LastName = txtLastName.Text.Trim();
            guest.Address = txtAdress.Text.Trim();
            guest.PhoneNr = txtPhone.Text.Trim();
            guest.Age = Convert.ToInt32(txtAge.Text.Trim());
            guest.Gender = txtGender.Text.Trim();
            guest.Country = txtCountry.Text.Trim();
            guest.City = txtCity.Text.Trim();
            guest.HotelRoom = Convert.ToInt32(txtHotelRoom.Text.Trim());
            guest.ArrivalDate = txtArrivalDate.Text.Trim();
            guest.DepartureDate = txtDepartureDate.Text.Trim();
            guest.Email = txtEmail.Text.Trim();
            //_guestService.InsertOrUpdate(); neradom kaip padaryti
            using (Hotel.Dal.HotelDbContext db = new Dal.HotelDbContext())
            {
                if (guest.ID == 0) //isertinti
                    db.Guests.Add(guest);
                else // updatinti
                {
                    db.Entry(guest).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            MessageBox.Show("Successful");
            Clear();
        }
    }
}
