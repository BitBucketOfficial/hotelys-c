﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.Dal;
using Hotel.Services;
using Microsoft.EntityFrameworkCore;

namespace Hotel.Presentation.Guest_and_Agents
{
    public partial class GuestList : Form
    {
        private readonly GuestService _guestService;
        public GuestList()
        {
            InitializeComponent();
            _guestService = new GuestService();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        void FullDataGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = _guestService.GetGuests().ToList();
        }

        private void GuestList_Load(object sender, EventArgs e)
        {
            FullDataGrid();
            foreach (var item in _guestService.GetGuests().OrderBy(x => x.HotelRoom)) // hotelrooms vietoj GetGuests()
            {
            cmbRoom.Items.Add(item.HotelRoom);
            }
            foreach (var item in _guestService.GetGuests().OrderBy(x => x.Age)) 
            {
                cmbAge.Items.Add(item.Age);
            }
            
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != -1)
            {
                var ID = Convert.ToInt32(dataGridView1.CurrentRow.Cells["ID"].Value);
                Modification mod = new Modification(ID);
                mod.ShowDialog();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void ckbMale_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbMale.Checked)
            {
                var db = _guestService.GetGuests();
                dataGridView1.AutoGenerateColumns = false;
                if (txtSearch.Text == string.Empty)
                {
                        dataGridView1.DataSource = db.Where(y => !y.Gender.Contains("Female")).ToList();
                }
                else
                {
                        dataGridView1.DataSource = db.Where(y => !y.Gender.Contains("Female") && y.Name.Contains(txtSearch.Text) || y.LastName.StartsWith(txtSearch.Text)).ToList();
                }
            }
            else
            {
                if (txtSearch.Text == string.Empty)
                {
                    FullDataGrid();
                }
                else
                {
                    FullDataGrid();
                    Search();
                }
            }
        }

        private void ckbFemale_CheckedChanged(object sender, EventArgs e)
        {
            var db = _guestService.GetGuests();
            if (ckbFemale.Checked)
            {
                if (txtSearch.Text == string.Empty)
                {
                        dataGridView1.DataSource = db.Where(x => x.Gender.Contains("Female")).ToList();
                }
                else
                {
                         dataGridView1.DataSource = db.Where(x => x.Gender.Contains("Female") &&  x.Name.Contains(txtSearch.Text) || x.LastName.StartsWith(txtSearch.Text)).ToList();
                }
            }
            else
            {
                if (txtSearch.Text == string.Empty)
                {
                FullDataGrid();
                }
                else
                {
                    FullDataGrid();
                    Search();
                }

            }
        }

        private void Search()
        {
            var db = _guestService.GetGuests();
            dataGridView1.AutoGenerateColumns = false;
                dataGridView1.DataSource = db.Where(x => x.Name.Contains(txtSearch.Text) || x.LastName.StartsWith(txtSearch.Text)).ToList();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) // cmdAge
        {
            dataGridView1.DataSource = _guestService.GetGuests().Where(x => x.Age == Convert.ToInt32(cmbAge.Text)).ToList();
        }

        private void cmbRoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = _guestService.GetGuests().Where(x => x.HotelRoom == Convert.ToInt32(cmbRoom.Text)).ToList();
        }
    }
}
