﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dal;
using Hotel.Dal.ViewModels;

namespace Hotel.Services
{
    public class UserService
    {
        private readonly HotelDbContext _db;
        public UserService()
        {
            _db = new HotelDbContext();
        }
        public LoginUser LoginUser(LoginUser loginUser)
        {
            var userByNickName = _db.Users.Where(m => m.Nickname == loginUser.Nickname).FirstOrDefault();

            if (userByNickName == null)
            {
                loginUser.LoginStatus = LoginStatus.wrongNick;
                return loginUser;
            }

            if (userByNickName.Password != loginUser.Password)
            {
                loginUser.LoginStatus = LoginStatus.wrongPassword;
                return loginUser;
            }

            loginUser.LoginStatus = LoginStatus.correct;
            return loginUser;
        }
        
    }
}
