﻿using Hotel.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Services
{
    public class RoomService : HotelDbContext
    {
        private readonly HotelDbContext _db;
        public RoomService()
        {
            _db = new HotelDbContext();
        }
        public List<Room> HotelRooms()
        {
            var duomenys = _db.Rooms.Select(x => new Room
            {
                ID = x.ID,
                RoomNumber = x.RoomNumber,
                RoomStatus = x.RoomStatus,
                RoomTypeID = x.RoomTypeID

            })
               //.Where()
               .ToList();
            return duomenys;
        }
    }
}
