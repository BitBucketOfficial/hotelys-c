﻿using Hotel.Dal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.Dal.ViewModels;
using Hotel.Services;
using Hotel.Presentation.Main;

namespace Hotel.Presentation.LogIn
{
    public partial class LogIn : Form
    {
        private readonly UserService _service;
        public LogIn()
        {
            _service = new UserService();
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            {

            }
            if (CheckIfValid())
            {
                LoginUser user = new LoginUser
                {
                    Nickname = txtNickName.Text,
                    Password = txtPassword.Text
                };

                if ((user = _service.LoginUser(user)).LoginStatus == LoginStatus.correct)
                {
                    this.Hide();
                    Main.Main main = new Main.Main();
                    main.Show();
                }
                else
                {
                    ChangeColor(user.LoginStatus);
                }
                
                
            }
        }
        private bool CheckIfValid()
        {

            if (string.IsNullOrWhiteSpace(txtNickName.Text))
            {
                MessageBox.Show("Write your Nickname");
                return false;
            }
            else if (string.IsNullOrWhiteSpace(txtPassword.Text))
            {
                MessageBox.Show("Write your Password");
                return false;
            }

            return true;
        }
        private void ChangeColor(LoginStatus? loginStatus)
        {
            if (loginStatus.HasValue && loginStatus.Value == LoginStatus.wrongNick)
            {
                txtNickName.BackColor = Color.Red;
                txtPassword.BackColor = Color.Red;
                txtPassword.Text = null;
                MessageBox.Show("Wrong Nickname or password");
            }

            if (loginStatus.HasValue && loginStatus.Value == LoginStatus.wrongPassword)
            {
                txtNickName.BackColor = Color.Red;
                txtPassword.BackColor = Color.Red;
                txtPassword.Text = null;
                MessageBox.Show("Wrong Nickname or password");
            }
        }

        private void LogIn_Load(object sender, EventArgs e)
        {

        }

        private void LogIn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter)
            {
               btnLogin.PerformClick();
            }
        }

        private void txtNickName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter)
            {
                btnLogin.PerformClick();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter)
            {
                btnLogin.PerformClick();
            }

        }
    }
}
