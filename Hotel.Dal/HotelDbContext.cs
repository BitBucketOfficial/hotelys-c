﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dal.DalExtensions;

namespace Hotel.Dal
{
    public class HotelDbContext : DbContext
    {
        public HotelDbContext()
            : base()
        {

        }
        public DbSet<Guest> Guests { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Room> Rooms { get; set; }
        //

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            


            modelBuilder.MapUser();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;database=Hotel;user=root;password=root");
        }
    }
}
