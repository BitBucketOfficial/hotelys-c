﻿using MySql.Data.MySqlClient;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal
{
    public class DBConnection : DbContext
    {
        public MySqlConnection Connection { get; set; }

        public DBConnection(string connectionName = "HotelConnectionString")
        {
            var connectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            Connection = new MySqlConnection(connectionString);
        }

        public void ChangeConnection(string newConnectionName)
        {
            Connection.ConnectionString = ConfigurationManager.ConnectionStrings[newConnectionName].ConnectionString;
        }
    }
}
