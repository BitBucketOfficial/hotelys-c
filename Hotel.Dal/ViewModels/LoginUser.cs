﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal.ViewModels
{
    public class LoginUser
    {
        public string Nickname { get; set; }

        public string Password { get; set; }

        public LoginStatus? LoginStatus { get; set; }
    }
}
