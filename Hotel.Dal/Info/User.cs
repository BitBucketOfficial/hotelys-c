﻿using Hotel.Dal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal
{
    public class User
    {
        public int ID { get; set; }

        public string Nickname { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }
    }
}
