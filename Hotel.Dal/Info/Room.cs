﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal
{
    public class Room
    {
        public int? ID { get; set; }

        public int RoomNumber { get; set; }

        public int RoomStatus { get; set; }

        public int RoomTypeID { get; set; }
    }
}
