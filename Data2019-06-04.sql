CREATE DATABASE  IF NOT EXISTS `hotel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `hotel`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: hotel
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblbookings`
--

DROP TABLE IF EXISTS `tblbookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblbookings` (
  `bBookingID` int(11) NOT NULL AUTO_INCREMENT,
  `bHotelID` int(11) DEFAULT NULL,
  `bGuestID` int(11) DEFAULT NULL,
  `bBookingStatusID` int(11) DEFAULT NULL,
  `bReservationAgentID` int(11) DEFAULT NULL,
  `bDateFrom` varchar(50) DEFAULT NULL,
  `bDateTo` varchar(50) DEFAULT NULL,
  `bRoomCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`bBookingID`),
  KEY `bHotelID` (`bHotelID`),
  KEY `bGuestID` (`bGuestID`),
  KEY `bBookingStatusID` (`bBookingStatusID`),
  KEY `bReservationAgentID` (`bReservationAgentID`),
  CONSTRAINT `tblbookings_ibfk_1` FOREIGN KEY (`bHotelID`) REFERENCES `tblhotels` (`hHotelID`),
  CONSTRAINT `tblbookings_ibfk_2` FOREIGN KEY (`bGuestID`) REFERENCES `tblguest` (`gID`),
  CONSTRAINT `tblbookings_ibfk_3` FOREIGN KEY (`bBookingStatusID`) REFERENCES `tblbookingstatus` (`bsBookingStatusID`),
  CONSTRAINT `tblbookings_ibfk_4` FOREIGN KEY (`bReservationAgentID`) REFERENCES `tblreservationagents` (`raID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblbookings`
--

LOCK TABLES `tblbookings` WRITE;
/*!40000 ALTER TABLE `tblbookings` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblbookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblbookingstatus`
--

DROP TABLE IF EXISTS `tblbookingstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblbookingstatus` (
  `bsBookingStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `bsStatus` int(11) DEFAULT NULL,
  `bsDiscription` varchar(225) DEFAULT NULL,
  `bsActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`bsBookingStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblbookingstatus`
--

LOCK TABLES `tblbookingstatus` WRITE;
/*!40000 ALTER TABLE `tblbookingstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblbookingstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblguest`
--

DROP TABLE IF EXISTS `tblguest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblguest` (
  `gID` int(11) NOT NULL AUTO_INCREMENT,
  `gName` varchar(50) DEFAULT NULL,
  `gLastName` varchar(50) DEFAULT NULL,
  `gAge` int(11) DEFAULT NULL,
  `gGender` varchar(10) DEFAULT NULL,
  `gPhoneNumber` varchar(20) DEFAULT NULL,
  `gAddress` varchar(100) DEFAULT NULL,
  `gCountry` varchar(50) DEFAULT NULL,
  `gCity` varchar(50) DEFAULT NULL,
  `gHotelRoom` int(11) DEFAULT NULL,
  `gArrivalDate` varchar(20) DEFAULT NULL,
  `gDepartureDate` varchar(20) DEFAULT NULL,
  `gEmail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`gID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblguest`
--

LOCK TABLES `tblguest` WRITE;
/*!40000 ALTER TABLE `tblguest` DISABLE KEYS */;
INSERT INTO `tblguest` VALUES (1,'Marija','Kurnovka',32,'Female','+370656235','Verkiu s. 14','Lithuania','Vilnius',101,'2018-05-06','2018-05-16','Markius50@gmail.com'),(2,'Jonas','Paulauskas',34,'Male','+370651635','Verkiu s. 14','Lithuania','Vilnius',101,'2018-05-06','2018-05-16','Polskius@gmail.com'),(3,'Michale','Bernaton',50,'Male','+454256235','Bruklin s. 80','UnitedStates','NewYork',102,'2018-05-08','2018-05-18','WarMachine@gmail.com'),(4,'Jackson','Overmor',22,'Male','+9520256235','Merki s. 4','Germany','Berlin',103,'2018-05-30','2018-06-05','Grandbro@gmail.com');
/*!40000 ALTER TABLE `tblguest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblhotels`
--

DROP TABLE IF EXISTS `tblhotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblhotels` (
  `hHotelID` int(11) NOT NULL AUTO_INCREMENT,
  `hHotelCode` varchar(100) DEFAULT NULL,
  `hName` varchar(50) DEFAULT NULL,
  `hMotto` varchar(225) DEFAULT NULL,
  `hAddress` varchar(100) DEFAULT NULL,
  `hCity` varchar(50) DEFAULT NULL,
  `hCountry` varchar(50) DEFAULT NULL,
  `hNumber` varchar(20) DEFAULT NULL,
  `hCompanyEmailAddress` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hHotelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblhotels`
--

LOCK TABLES `tblhotels` WRITE;
/*!40000 ALTER TABLE `tblhotels` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblhotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblreservationagents`
--

DROP TABLE IF EXISTS `tblreservationagents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblreservationagents` (
  `raID` int(11) NOT NULL AUTO_INCREMENT,
  `raName` varchar(50) DEFAULT NULL,
  `raLastName` varchar(50) DEFAULT NULL,
  `raAddress` varchar(100) DEFAULT NULL,
  `raCountry` varchar(50) DEFAULT NULL,
  `raCity` varchar(50) DEFAULT NULL,
  `raPhoneNumber` varchar(20) DEFAULT NULL,
  `raGender` varchar(10) DEFAULT NULL,
  `raEmail` varchar(100) DEFAULT NULL,
  `raAge` int(11) DEFAULT NULL,
  PRIMARY KEY (`raID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblreservationagents`
--

LOCK TABLES `tblreservationagents` WRITE;
/*!40000 ALTER TABLE `tblreservationagents` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblreservationagents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblrooms`
--

DROP TABLE IF EXISTS `tblrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblrooms` (
  `rRoomID` int(11) NOT NULL AUTO_INCREMENT,
  `rHotelID` int(11) DEFAULT NULL,
  `rFloor` int(11) DEFAULT NULL,
  `rRoomTypeID` int(11) DEFAULT NULL,
  `rRoomNumber` int(11) DEFAULT NULL,
  `rDescription` varchar(225) DEFAULT NULL,
  `rRoomStatusID` int(11) DEFAULT NULL,
  PRIMARY KEY (`rRoomID`),
  KEY `rRoomStatusID` (`rRoomStatusID`),
  KEY `rRoomTypeID` (`rRoomTypeID`),
  KEY `rHotelID` (`rHotelID`),
  CONSTRAINT `tblrooms_ibfk_1` FOREIGN KEY (`rRoomStatusID`) REFERENCES `tblroomstatus` (`rsRoomStatusID`),
  CONSTRAINT `tblrooms_ibfk_2` FOREIGN KEY (`rRoomTypeID`) REFERENCES `tblroomtypes` (`rtRoomTypeID`),
  CONSTRAINT `tblrooms_ibfk_3` FOREIGN KEY (`rHotelID`) REFERENCES `tblhotels` (`hHotelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblrooms`
--

LOCK TABLES `tblrooms` WRITE;
/*!40000 ALTER TABLE `tblrooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblroomsbooked`
--

DROP TABLE IF EXISTS `tblroomsbooked`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblroomsbooked` (
  `rbRoomBookedID` int(11) NOT NULL AUTO_INCREMENT,
  `rbBookingId` int(11) DEFAULT NULL,
  `rbRoomID` int(11) DEFAULT NULL,
  PRIMARY KEY (`rbRoomBookedID`),
  KEY `rbBookingId` (`rbBookingId`),
  CONSTRAINT `tblroomsbooked_ibfk_1` FOREIGN KEY (`rbBookingId`) REFERENCES `tblbookings` (`bBookingID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblroomsbooked`
--

LOCK TABLES `tblroomsbooked` WRITE;
/*!40000 ALTER TABLE `tblroomsbooked` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblroomsbooked` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblroomstatus`
--

DROP TABLE IF EXISTS `tblroomstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblroomstatus` (
  `rsRoomStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `rsRoomStatus` tinyint(1) DEFAULT NULL,
  `rsDescription` varchar(225) DEFAULT NULL,
  `rsActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`rsRoomStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblroomstatus`
--

LOCK TABLES `tblroomstatus` WRITE;
/*!40000 ALTER TABLE `tblroomstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblroomstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblroomtypes`
--

DROP TABLE IF EXISTS `tblroomtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblroomtypes` (
  `rtRoomTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `rtRoomType` varchar(50) DEFAULT NULL,
  `rtDescription` varchar(225) DEFAULT NULL,
  `rtActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`rtRoomTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblroomtypes`
--

LOCK TABLES `tblroomtypes` WRITE;
/*!40000 ALTER TABLE `tblroomtypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblroomtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblstaff`
--

DROP TABLE IF EXISTS `tblstaff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblstaff` (
  `sStaffID` int(11) NOT NULL AUTO_INCREMENT,
  `sFirstName` varchar(50) DEFAULT NULL,
  `sLastName` varchar(50) DEFAULT NULL,
  `sAddress` varchar(225) DEFAULT NULL,
  `sCity` varchar(50) DEFAULT NULL,
  `sCountry` varchar(50) DEFAULT NULL,
  `sPhoneNumber` varchar(20) DEFAULT NULL,
  `sEmail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sStaffID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblstaff`
--

LOCK TABLES `tblstaff` WRITE;
/*!40000 ALTER TABLE `tblstaff` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblstaff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblstaffrooms`
--

DROP TABLE IF EXISTS `tblstaffrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblstaffrooms` (
  `srStaffRoomID` int(11) NOT NULL AUTO_INCREMENT,
  `srRoomID` int(11) DEFAULT NULL,
  `srStaffID` int(11) DEFAULT NULL,
  PRIMARY KEY (`srStaffRoomID`),
  KEY `srRoomID` (`srRoomID`),
  KEY `srStaffID` (`srStaffID`),
  CONSTRAINT `tblstaffrooms_ibfk_1` FOREIGN KEY (`srRoomID`) REFERENCES `tblrooms` (`rRoomID`),
  CONSTRAINT `tblstaffrooms_ibfk_2` FOREIGN KEY (`srStaffID`) REFERENCES `tblstaff` (`sStaffID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblstaffrooms`
--

LOCK TABLES `tblstaffrooms` WRITE;
/*!40000 ALTER TABLE `tblstaffrooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblstaffrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbluser`
--

DROP TABLE IF EXISTS `tbluser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbluser` (
  `uID` int(11) NOT NULL AUTO_INCREMENT,
  `uNickname` varchar(50) DEFAULT NULL,
  `uPassword` varchar(50) DEFAULT NULL,
  `uEmail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`uID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbluser`
--

LOCK TABLES `tbluser` WRITE;
/*!40000 ALTER TABLE `tbluser` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbluser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'hotel'
--

--
-- Dumping routines for database 'hotel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-04 17:00:40
