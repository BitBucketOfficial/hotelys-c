-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: hotel
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblbookings`
--

DROP TABLE IF EXISTS `tblbookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblbookings` (
  `bBookingID` int(11) NOT NULL AUTO_INCREMENT,
  `bHotelID` int(11) DEFAULT NULL,
  `bGuestID` int(11) DEFAULT NULL,
  `bBookingStatusID` int(11) DEFAULT NULL,
  `bReservationAgentID` int(11) DEFAULT NULL,
  `bDateFrom` varchar(50) DEFAULT NULL,
  `bDateTo` varchar(50) DEFAULT NULL,
  `bRoomCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`bBookingID`),
  KEY `bHotelID` (`bHotelID`),
  KEY `bGuestID` (`bGuestID`),
  KEY `bBookingStatusID` (`bBookingStatusID`),
  KEY `bReservationAgentID` (`bReservationAgentID`),
  CONSTRAINT `tblbookings_ibfk_1` FOREIGN KEY (`bHotelID`) REFERENCES `tblhotels` (`hHotelID`),
  CONSTRAINT `tblbookings_ibfk_2` FOREIGN KEY (`bGuestID`) REFERENCES `tblguest` (`gID`),
  CONSTRAINT `tblbookings_ibfk_3` FOREIGN KEY (`bBookingStatusID`) REFERENCES `tblbookingstatus` (`bsBookingStatusID`),
  CONSTRAINT `tblbookings_ibfk_4` FOREIGN KEY (`bReservationAgentID`) REFERENCES `tblreservationagents` (`raID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblbookings`
--

LOCK TABLES `tblbookings` WRITE;
/*!40000 ALTER TABLE `tblbookings` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblbookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblbookingstatus`
--

DROP TABLE IF EXISTS `tblbookingstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblbookingstatus` (
  `bsBookingStatusID` int(11) NOT NULL AUTO_INCREMENT,
  `bsStatus` int(11) DEFAULT NULL,
  `bsDiscription` varchar(225) DEFAULT NULL,
  `bsActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`bsBookingStatusID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblbookingstatus`
--

LOCK TABLES `tblbookingstatus` WRITE;
/*!40000 ALTER TABLE `tblbookingstatus` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblbookingstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblguest`
--

DROP TABLE IF EXISTS `tblguest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblguest` (
  `gID` int(11) NOT NULL AUTO_INCREMENT,
  `gName` varchar(50) DEFAULT NULL,
  `gLastName` varchar(50) DEFAULT NULL,
  `gAge` int(11) DEFAULT NULL,
  `gGender` varchar(10) DEFAULT NULL,
  `gPhoneNumber` varchar(20) DEFAULT NULL,
  `gAddress` varchar(100) DEFAULT NULL,
  `gCountry` varchar(50) DEFAULT NULL,
  `gCity` varchar(50) DEFAULT NULL,
  `gHotelRoom` int(11) DEFAULT NULL,
  `gArrivalDate` varchar(20) DEFAULT NULL,
  `gDepartureDate` varchar(20) DEFAULT NULL,
  `gEmail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`gID`),
  UNIQUE KEY `gHotelRoom` (`gHotelRoom`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblguest`
--

LOCK TABLES `tblguest` WRITE;
/*!40000 ALTER TABLE `tblguest` DISABLE KEYS */;
INSERT INTO `tblguest` VALUES (1,'Marija','Kurnovka',32,'Female','+370656235','Verkiu s. 14','Lithuania','Vilnius',104,'2018-05-06','2018-05-16','Markius50@gmail.com'),(2,'Jonas','Paulauskas',34,'Male','+370651635','Verkiu s. 14','Lithuania','Vilnius',101,'2018-05-06','2018-05-16','Polskius@gmail.com'),(3,'Michale','Bernaton',50,'Male','+454256235','Bruklin s. 80','UnitedStates','NewYork',102,'2018-05-08','2018-05-18','WarMachine@gmail.com'),(4,'Jackson','Overmor',22,'Male','+9520256235','Merki s. 4','Germany','Berlin',103,'2018-05-30','2018-06-05','Grandbro@gmail.com');
/*!40000 ALTER TABLE `tblguest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblhotels`
--

DROP TABLE IF EXISTS `tblhotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblhotels` (
  `hHotelID` int(11) NOT NULL AUTO_INCREMENT,
  `hHotelCode` varchar(100) DEFAULT NULL,
  `hName` varchar(50) DEFAULT NULL,
  `hMotto` varchar(225) DEFAULT NULL,
  `hAddress` varchar(100) DEFAULT NULL,
  `hCity` varchar(50) DEFAULT NULL,
  `hCountry` varchar(50) DEFAULT NULL,
  `hNumber` varchar(20) DEFAULT NULL,
  `hCompanyEmailAddress` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hHotelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblhotels`
--

LOCK TABLES `tblhotels` WRITE;
/*!40000 ALTER TABLE `tblhotels` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblhotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblreservationagents`
--

DROP TABLE IF EXISTS `tblreservationagents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblreservationagents` (
  `raID` int(11) NOT NULL AUTO_INCREMENT,
  `raName` varchar(50) DEFAULT NULL,
  `raLastName` varchar(50) DEFAULT NULL,
  `raAddress` varchar(100) DEFAULT NULL,
  `raCountry` varchar(50) DEFAULT NULL,
  `raCity` varchar(50) DEFAULT NULL,
  `raPhoneNumber` varchar(20) DEFAULT NULL,
  `raGender` varchar(10) DEFAULT NULL,
  `raEmail` varchar(100) DEFAULT NULL,
  `raAge` int(11) DEFAULT NULL,
  PRIMARY KEY (`raID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblreservationagents`
--

LOCK TABLES `tblreservationagents` WRITE;
/*!40000 ALTER TABLE `tblreservationagents` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblreservationagents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblroom`
--

DROP TABLE IF EXISTS `tblroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblroom` (
  `rID` int(11) NOT NULL AUTO_INCREMENT,
  `rRoomStatus` tinyint(1) DEFAULT '0',
  `rTypeID` int(11) DEFAULT NULL,
  `rRoomNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`rID`),
  UNIQUE KEY `rRoomNumber` (`rRoomNumber`),
  KEY `rTypeID` (`rTypeID`),
  CONSTRAINT `tblroom_ibfk_1` FOREIGN KEY (`rTypeID`) REFERENCES `tblroomtype` (`rtID`),
  CONSTRAINT `tblroom_ibfk_2` FOREIGN KEY (`rRoomNumber`) REFERENCES `tblroomnumber` (`rnNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblroom`
--

LOCK TABLES `tblroom` WRITE;
/*!40000 ALTER TABLE `tblroom` DISABLE KEYS */;
INSERT INTO `tblroom` VALUES (1,1,1,101),(2,1,3,102),(3,1,2,103),(4,1,5,104);
/*!40000 ALTER TABLE `tblroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblroomnumber`
--

DROP TABLE IF EXISTS `tblroomnumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblroomnumber` (
  `rnID` int(11) NOT NULL AUTO_INCREMENT,
  `rnNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`rnID`),
  UNIQUE KEY `rnNumber` (`rnNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblroomnumber`
--

LOCK TABLES `tblroomnumber` WRITE;
/*!40000 ALTER TABLE `tblroomnumber` DISABLE KEYS */;
INSERT INTO `tblroomnumber` VALUES (1,101),(2,102),(3,103),(4,104),(5,105),(6,201),(7,202),(8,203),(9,204),(10,205),(11,301),(12,302),(13,303),(14,304),(15,305);
/*!40000 ALTER TABLE `tblroomnumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblroomtype`
--

DROP TABLE IF EXISTS `tblroomtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblroomtype` (
  `rtID` int(11) NOT NULL AUTO_INCREMENT,
  `rtRoomType` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`rtID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblroomtype`
--

LOCK TABLES `tblroomtype` WRITE;
/*!40000 ALTER TABLE `tblroomtype` DISABLE KEYS */;
INSERT INTO `tblroomtype` VALUES (1,'Simple'),(2,'Better'),(3,'TheBest'),(4,'Luxury'),(5,'Extra');
/*!40000 ALTER TABLE `tblroomtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblstaff`
--

DROP TABLE IF EXISTS `tblstaff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblstaff` (
  `sStaffID` int(11) NOT NULL AUTO_INCREMENT,
  `sFirstName` varchar(50) DEFAULT NULL,
  `sLastName` varchar(50) DEFAULT NULL,
  `sAddress` varchar(225) DEFAULT NULL,
  `sCity` varchar(50) DEFAULT NULL,
  `sCountry` varchar(50) DEFAULT NULL,
  `sPhoneNumber` varchar(20) DEFAULT NULL,
  `sEmail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sStaffID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblstaff`
--

LOCK TABLES `tblstaff` WRITE;
/*!40000 ALTER TABLE `tblstaff` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblstaff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbluser`
--

DROP TABLE IF EXISTS `tbluser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbluser` (
  `uID` int(11) NOT NULL AUTO_INCREMENT,
  `uNickname` varchar(50) DEFAULT NULL,
  `uPassword` varchar(50) DEFAULT NULL,
  `uEmail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`uID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbluser`
--

LOCK TABLES `tbluser` WRITE;
/*!40000 ALTER TABLE `tbluser` DISABLE KEYS */;
INSERT INTO `tbluser` VALUES (1,'admin','admin',NULL);
/*!40000 ALTER TABLE `tbluser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'hotel'
--

--
-- Dumping routines for database 'hotel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-27 14:10:51
